## Configure a CRUD APP with Node JS

### References

https://www.youtube.com/watch?v=voDummz1gO0
https://github.com/CodAffection/Node.js-Expess-MongoDB-CRUD/tree/master/project/views

### Up and Running Application

For run CRUD Node.js, first clone this repo.

```
git clone https://renizgo@bitbucket.org/renizgo/challenge-rest.git
```

Inside directory nodejs, run Docker Compose

```
cd nodejs
docker-compose up -d
```
 
I could not run Mongo and create Database in Docker Compose file, therefore it is necessary the next step

Open browser in [http://localhost:8081](http://localhost:8081) and create database ```EmployeeDB```

![Image01](/nodejs/images/image01.png)

![Image02](/nodejs/images/image02.png)

Restart Node.js container in Docker Compose

```
docker-compose restart node
```

After that, open application in your browser [http://localhost/employee](http://localhost/employee) 

![Image03](/nodejs/images/image03.png)

![Image04](/nodejs/images/image04.png)

Can you delete or exclude employeee.

Enjoy App!!!