const mongoose = require('mongoose');

mongoose.connect('mongodb://root:example@localhost:27017/EmployeeDB?authSource=admin', { useNewUrlParser: true }, (err) => {
    if (!err) { console.log('MongoDB Connection Succeeded.') }
    else { console.log('Error in DB connection : ' + err) }
});

require('./employee.model');