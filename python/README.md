# Learning a little bit Python

## Objectives

I created this document to show my learning path a simple CRUD Python application.
CRUD is (Create, Read, Update and Delete), this is a simple REST API.

I will show how to was constructed

## References

I used how references this [vídeo do Youtube](https://www.youtube.com/watch?v=N6cZ6aHvLYs), from Rafael Marques. This video show us a overview very cool about create a CRUD API REST in Python.

To see the Rafael Code use [this link](https://github.com/ceb10n/simples-flask-api).

## Requirements

Install the virtualenv:

```
pip install virtualenv
```

![Image01](/python/images/image01.png)

Create a work directory:

```
mkdir simple-flask-api
cd simple-flask-api
```

The next steps create a virtual enviroment, install dependencies the Python Flask and create requirements.txt file, with a correct especifications 


```
virtualenv .venv
source .venv/bin/activate
pip install flask
```

Will show this message:

```
Successfully installed Jinja2-2.10.1 MarkupSafe-1.1.1 Werkzeug-0.15.4 click-7.0 flask-1.1.1 itsdangerous-1.1.0
```

Create requirement file:

```
echo "flask==1.1.1 > requirements.txt
```

Now, creating the files for our applicattion.

## Step by step to create application

### Step 1

Just a simple Flask Application running:

Code

![Image02](/python/images/image02.png)

Run App

![Image03](/python/images/image03.png)

App running

![Image04](/python/images/image04.png)

### Step 2

*The GET Method*

In this version will add a database in memory in a ```.json``` format and send a GET method to retrieve data.

Code

![Image05](/python/images/image05.png)

Return

![Image06](/python/images/image06.png)

### Step 3

*The GET Method, filter per programing languages*

Code

![Image07](/python/images/image07.png)

Return

![Image08](/python/images/image08.png)

Filter per Python

![Image09](/python/images/image09.png)

### Step 4

*The GET Method, filter by ID*

Code

![Image10](/python/images/image10.png)

Results

![Image11](/python/images/image11.png)

![Image12](/python/images/image12.png)

![Image13](/python/images/image13.png)

### Step 5

*The POST Method* 

Add request in first line the ```app/py```

```
from flask import Flask, jsonify, request
```

Code

![Image14](/python/images/image14.png)

The POST Method

![Image15](/python/images/image15.png)

The GET Method

![Image16](/python/images/image16.png)


### Step 6

*The PUT Method, Changing language inserting ID in request*

Code

![Image17](/python/images/image17.png)

Results

The GET Method

![Image18](/python/images/image18.png)

The PUT Method

![Image19](/python/images/image19.png)

The GET Method

![Image20](/python/images/image20.png)

### Step 7

*The DELETE Method*

Code

![Image21](/python/images/image21.png)

The GET Method

![Image22](/python/images/image22.png)

The DELETE Method

![Image23](/python/images/image23.png)

The GET Method 

![Image24](/python/images/image24.png)

![Image25](/python/images/image25.png)

