#!/bin/bash

echo "Changing Localhost for IP Server in nginx.conf file"
IP=`ifconfig | grep -A2 enp | grep -v inet6 | grep inet | awk '{print $2}'`
sed -i -e "s/localhost/$IP/" nginx-config/nginx.conf

# Verifying instalation of Docker
echo "verifying Docker installation"
sudo docker --version 1> /dev/null 2> /dev/stdout
echo "----------"
if [ $? -ne 0 ]; then
  echo "Docker OK"
  echo "Docker is installed"
else
  echo "Installing Docker"
  sudo apt-get update -y
  sudo apt-get install -y \
    apt-transport-https \
    ca-certificates \
    curl \
    software-properties-common
  curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
  sudo add-apt-repository \
   "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
   $(lsb_release -cs) \
   stable"
  sudo apt-get update -y
  sudo apt-get install -y docker-ce
  echo "----------"
  sudo docker --version
  echo "----------"
  echo "Above appears docker version?"
  sudo apt install -y docker-compose
  sudo docker-compose --version
  echo "----------"
  echo "Above appears docker version? Yes or No"
fi

echo "Configuring Docker permission for use Docker without SUDO"

sudo groupadd docker
sudo usermod -aG docker $USER
sudo mkdir /home/"$USER"/.docker
sudo chown "$USER":"$USER" /home/"$USER"/.docker -R
sudo chmod g+rwx "$HOME/.docker" -R

echo "Disable Firewall for tests"
sudo ufw disable

git clone https://bitbucket.org/renizgo/challenge-rest.git

crontab -l 2>/dev/null; echo "@reboot /usr/bin/docker-compose -f /home/$USER/challenge-rest/terraform/docker-compose.yml  up -d" | crontab -

echo "Your server is restarting..."
echo "Rebooting now"
sudo reboot

