output "aws_instance_public_dns" {
value = aws_instance.nginx.public_dns
}

output "aws_instance_public_ip" {
value = aws_instance.nginx.public_ip
}

output "aws_eip_public_ip" {
value = aws_eip.ip-test-env.public_ip
}

output "aws_eip_public_dns" {
value = aws_eip.ip-test-env.public_dns
}

output "aws_security_group_id" {
    value = aws_security_group.ingress-all-test.id
} 