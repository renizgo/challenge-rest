locals {
  conn_type    = "ssh"
  conn_user    = "ubuntu"
  conn_timeout = "1m"
  conn_key = "${file("/Users/renato/Documents/aws-keys/renizgo.pem")}"
  conn_host         = "self.public_ip"
}

resource "aws_instance" "nginx" {
ami = "ami-07b4f3c02c7f83d59"
instance_type = "t2.micro"
key_name = var.key_name
#vpc_security_group_ids = ["${aws_security_group.ingress-all-test.id}"]
#security_groups = [ "ingress-all-test" ]
#security_groups = [ "allow-all-sg" ]

/*
provisioner "file" {
    source      = "install.sh"
    destination = "/home/ubuntu/install.sh"

    connection {
      type        = "${local.conn_type}"
      user        = "${local.conn_user}"
      timeout     = "${local.conn_timeout}"
      private_key = "${local.conn_key}"
      host        = "${local.conn_host}"
    }
  }

  provisioner "remote-exec" {
    inline = [
      "sleep 20",
      "cd /home/ubuntu/challenge-rest/terraform_study/",
      "chmod +x install.sh",
      "sudo ./instal.sh",
      "sleep 20",
    ]

    connection {
      type        = "${local.conn_type}"
      user        = "${local.conn_user}"
      timeout     = "${local.conn_timeout}"
      private_key = "${local.conn_key}"
      host        = "${local.conn_host}"
    }
  }
*/
}

