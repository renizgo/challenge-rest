# Running application with Terraform EC2 Instance

For run app in AWS, run these commands:

```
git clone https://renizgo@bitbucket.org/renizgo/challenge-rest.git
cd challeng-rest/terraform
```

Run Terraform commands:

```
terraform init
terraform plan -var-file=terraform.tfvars
terraform apply -var-file=terraform.tfvars --auto-approve
```

Unfortunately I could not automate 100%, I could not copy copy and execute script in created instance.

I set it but did not do it. :(

![Image01](/terraform/images/image01.png)

![Image02](/terraform/images/image02.png)

When run commands above, The following messages appear:

![Image03](/terraform/images/image03.png)

Copy install.sh script to EC2 Instance:

```
scp -i ~/Documents/aws-keys/renizgo.pem install.sh ubuntu@35.164.179.78:/home/ubuntu
```

Access your EC2 instance:

![Image04](/terraform/images/image04.png)

Execute install.sh script:

```
chmod +x install.sh
./install.sh
``` 

Wait some minutes and access instance again and run theses commands:

```
cd /home/ubuntu/challenge-rest/terraform/
docker-compose ps
```

![Image05](/terraform/images/image05.png)


Now, you can access your api:

![Image06](/terraform/images/image06.png)

You can make requests http in your application

![Image07](/terraform/images/image07.png)

You simple application it`s done





