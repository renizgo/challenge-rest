# A simple example a scheduler in Python

## Requirements

```
sudo pip3 install schedule
```

## Script in Python

This is my script:

```python
import schedule
import os
def job():
    print("A Simple Python Scheduler.")
    curlCmd = 'curl -s http://18.237.115.248/devs > results.txt'
    os.system(curlCmd)
    print("Now a number this developers Python is:")
    catCmd = 'cat results.txt | grep python | wc -l'
    os.system(catCmd)
# run the function job() every 2 seconds
schedule.every(2).seconds.do(job)

while True:
    schedule.run_pending()
```

Run script:

```
python3 scheduler.py
```

![Image01](/scheduler/images/image01.png)

Insert one Deleloper Python

![Image02](/scheduler/images/image02.png)

Verify results in script Python

![Image03](/scheduler/images/image03.png)

