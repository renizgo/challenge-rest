import schedule  
import os  
def job():  
    print("A Simple Python Scheduler.")  
    curlCmd = 'curl -s http://18.237.115.248/devs > results.txt'
    os.system(curlCmd)
    print("Now a number this developers Python is:")
    catCmd = 'cat results.txt | grep python | wc -l'
    os.system(catCmd)  
# run the function job() every 2 seconds  
schedule.every(2).seconds.do(job)  
  
while True:  
    schedule.run_pending()  
