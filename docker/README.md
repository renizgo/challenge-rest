# CRUD APP Python in Docker

## Objectives

In this repository I will show a simple application Python with Flask, when us can get a Rest API calls, to one CRUD application.

## Up and Runnig application with Docker

For generate image Docker:

```
docker build -t renizgo/challenge-docker .
```

For the run application

```
docker run -p 5000:5000 renizgo/challenge-docker
```

![Image01](/docker/images/image01.png)

The GET Method

![Image02](/docker/images/image02.png)


## Use a Postman program to explore more methods in our REST API

Use GET method to list developers inserted in DB

![Image03](/docker/images/image03.png)

Use the POST method to add a developer

![Image04](/docker/images/image04.png)

![Image05](/docker/images/image05.png)

Use PUT method to change one language programing

![Image06](/docker/images/image06.png)

![Image07](/docker/images/image07.png)

Use DELETE method to delete one developer

![Image08](/docker/images/image08.png)

![Image09](/docker/images/image09.png)

