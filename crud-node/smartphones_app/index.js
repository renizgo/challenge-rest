// index.js
const express = require('express');
const bodyParser = require('body-parser');

// Import routes
const smartphone = require('./routes/smartphones.route');
const app = express();

// DB Access
const mongoose = require('mongoose');
//mongodb+srv://bd_user:<password>@cluster0-m438h.gcp.mongodb.net/test?retryWrites=true&w=majority
let url = 'mongodb://db_user:123mongo@cluster0-m438h.gcp.mongodb.net:27017/smartphones';
let mongoDB = process.env.MONGODB_URI || url;
mongoose.connect(mongoDB,{useNewUrlParser: true });
mongoose.Promise = global.Promise;

let db = mongoose.connection;
db.on('error', console.error.bind(console, 'Erro na Ligação ao MongoDB'));

//Body Parser
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));
//app.use('/smartphones', smartphone);
app.use('/', smartphone);

// Associate port to app
let porto = 8000;
app.listen(porto, () => {
    console.log('Server is executing in port: ' + porto);
});

