# CRUD - Create CRUD Application with Node JS

## References

[Tutorial - Medium](https://medium.com/@pedrompinto/criar-uma-app-crud-com-node-js-mongodb-9cdc703000ae)

## Prepare infrastructure to CRUD Application

Create one directory to work

```
mkdir smartphones_app
cd smartphones_app/
```

Start a project Node Js

```
npm init
```

We will install these modules: Express.js, Body Parser, Mongoose

Express Js  = Framework Web Application
Body-Parser = Package to manipulate requests JSON
Mogoose     = Pakage to comunicate with MongoDB

```
npm install --save express body-parser mongoose
```

## Create a server CRUD Application

Create a ```index.js``` file with this content

In this moment we use just Express.js and Body-parser dependencies

View file index.js

```
// index.js

const express = require('express');
const bodyParser = require('body-parser');

// Initiate App Express

const app = express();

// Associate port to app

let porto = 8000;
app.listen(porto, () => {
    console.log('Server is executing in port: ' + porto);
});
```

![Image01](/crud-node/images/image01.png)

![Image02](/crud-node/images/image02.png)

