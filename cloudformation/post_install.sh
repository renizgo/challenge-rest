#!/bin/bash
sudo yum install git -y
git clone https://bitbucket.org/renizgo/challenge-rest.git
crontab -l 2>/dev/null; echo "@reboot /usr/bin/docker-compose -f /home/ec2-user/challenge-rest/terraform/docker-compose.yml  up -d" | crontab -
echo "Your server is restarting..."
echo "Rebooting now"
sudo reboot
